<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Message\Topic;

use Piotrbe\RabbitMqBundle\Base\BaseTopicMessage;

class TopicMessage extends BaseTopicMessage
{
    public function __construct(string $routingKey, protected array $data)
    {
        parent::__construct($routingKey);
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function jsonSerialize()
    {
        return [
            'routing_key' => $this->routingKey,
            'data' => $this->data
        ];
    }
}