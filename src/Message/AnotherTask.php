<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Message;

use Piotrbe\RabbitMqBundle\Base\BaseMessage;

class AnotherTask extends BaseMessage
{
    public function __construct(protected string $text)
    {
        parent::__construct('another_task');
    }

    public function jsonSerialize()
    {
        return [
            'type' => $this->getType(),
            'text' => $this->text
        ];
    }
}