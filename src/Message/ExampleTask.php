<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Message;

use Piotrbe\RabbitMqBundle\Base\BaseMessage;

class ExampleTask extends BaseMessage
{
    public function __construct(protected string $text)
    {
        parent::__construct('example_task');
    }

    public function jsonSerialize()
    {
        return [
            'type' => $this->getType(),
            'text' => $this->text
        ];
    }
}