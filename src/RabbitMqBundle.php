<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle;

use Piotrbe\RabbitMqBundle\DependencyInjection\PiotrbeRabbitMqExtension;
use Piotrbe\RabbitMqBundle\DependencyInjection\TaskPass;
use Piotrbe\RabbitMqBundle\DependencyInjection\TopicsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class RabbitMqBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new PiotrbeRabbitMqExtension();
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TaskPass());
        $container->addCompilerPass(new TopicsPass());
    }

}