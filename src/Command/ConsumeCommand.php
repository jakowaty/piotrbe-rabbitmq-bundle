<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Command;

use Piotrbe\RabbitMqBundle\Base\BaseConsumer;
use Piotrbe\RabbitMqBundle\Factory\AMQPConnectionAwareFactory;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsumeCommand extends Command
{
    public function __construct(private QueuesManager $queuesManager, private AMQPConnectionAwareFactory $factory)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('consumer:consume')
            ->setDescription('Process messages from queues')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Provide type of messages to be consumed'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');

        if (!$this->queuesManager->isMessageTypeAllowed($type)) {
            $output->writeln("Invalid queue name");
            return false;
        }

        $consumerClass = $this->queuesManager->getConsumerClassForMessageType($type);

        /** @var BaseConsumer $worker */
        $worker = $this->factory->createConnectionAwareService($consumerClass);
        $worker->work();

        return true;
    }
}