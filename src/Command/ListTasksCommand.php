<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Command;

use Piotrbe\RabbitMqBundle\Base\BaseConsumer;
use Piotrbe\RabbitMqBundle\Factory\AMQPConnectionAwareFactory;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListTasksCommand extends Command
{
    public function __construct(private QueuesManager $queuesManager)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('consumer:task:list-tasks')
            ->setDescription('Show list of tasks that can be consumed by "consumer:consume" command.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("ALLOWED TASKS FOR consumer:consume COMMAND:");
        foreach ($this->queuesManager->getExecutionMap() as $task => $class) {
            $output->writeln("$task ==> served by class '$class'");
        }

        return true;
    }
}