<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Command;

use Piotrbe\RabbitMqBundle\Base\BaseConsumer;
use Piotrbe\RabbitMqBundle\Base\BaseTopicConsumer;
use Piotrbe\RabbitMqBundle\Factory\AMQPConnectionAwareFactory;
use Piotrbe\RabbitMqBundle\Service\TopicsRouting;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsumeTopicCommand extends Command
{
    private const ARG_REQUIRED_TOPIC = 'topic';

    public function __construct(private TopicsRouting $topicsRouting, private AMQPConnectionAwareFactory $factory)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('consumer:topic:consume')
            ->setDescription('Process messages from user selected topic')
            ->addArgument(
                self::ARG_REQUIRED_TOPIC,
                InputArgument::REQUIRED,

            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $topic = $input->getArgument(self::ARG_REQUIRED_TOPIC);

        if (!$this->topicsRouting->isClassForTopicRoutingKey($topic)) {
            $output->writeln("Invalid topic routing keys");
            return false;
        }

        $consumerClass = $this->topicsRouting->getConsumerClassForRoutingKey($topic);

        /** @var BaseTopicConsumer $worker */
        $worker = $this->factory->createConnectionAwareService($consumerClass);
        $worker->work();

        return true;
    }
}