<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Command;

use Piotrbe\RabbitMqBundle\Service\TopicsRouting;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListTopicsCommand extends Command
{
    public function __construct(private TopicsRouting $topicsRouting)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('consumer:topic:list-topics')
            ->setDescription('Shows list of topics that can be consumed by "consumer:topic:consume" command');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("ALLOWED TOPICS LIST (derived from Topic attributes):");

        foreach ($this->topicsRouting->getWorkerToRoutingKeyMap() as $class => $routingKey) {
            $output->writeln("$routingKey  =>  served by class '$class");
        }

        return true;
    }
}