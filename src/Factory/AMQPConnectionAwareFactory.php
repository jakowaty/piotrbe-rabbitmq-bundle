<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Factory;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Piotrbe\RabbitMqBundle\Base\AMQPConnectionAware;
use Piotrbe\RabbitMqBundle\Base\BaseConsumer;
use Piotrbe\RabbitMqBundle\Base\BasePublisher;
use Piotrbe\RabbitMqBundle\Base\BaseTopicConsumer;
use Piotrbe\RabbitMqBundle\Base\BaseTopicPublisher;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;
use Piotrbe\RabbitMqBundle\Service\TopicsRouting;

class AMQPConnectionAwareFactory
{
    private AMQPStreamConnection $connection;

    public function __construct(
        string $host,
        int $port,
        string $user,
        string $password,
        private QueuesManager $queuesManager,
        private TopicsRouting $topicsRouting
    )
    {
        $this->createConnection($host, $port, $user, $password);
    }

    public function createConnectionAwareService(string $class): AMQPConnectionAware
    {
        switch ($class) {
            case ($this->isServiceCreationAllowed($class)):
                return $this->createService($this->connection, $class);
            default:
                throw new \InvalidArgumentException("Invalid service class: '$class' ");
        }
    }

    private function createService(AMQPStreamConnection $connection, string $class): AMQPConnectionAware
    {
        if (is_subclass_of($class, BaseConsumer::class) || is_subclass_of($class,BasePublisher::class)) {
            return new $class($connection, $this->queuesManager);
        }

        return new $class($connection, $this->topicsRouting);
    }

    private function createConnection(string $host, int $port, string $user, string $password): void
    {
        $this->connection = new AMQPStreamConnection($host, $port, $user,$password);
    }

    private function isServiceCreationAllowed(string $class): bool
    {
        return class_exists($class) &&
        (
            is_subclass_of($class, BaseConsumer::class) ||
            is_subclass_of($class, BasePublisher::class) ||
            is_subclass_of($class, BaseTopicConsumer::class) ||
            is_subclass_of($class, BaseTopicPublisher::class)
        );
    }
}