<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Consumer;

use Piotrbe\RabbitMqBundle\Annotation\Task;
use Piotrbe\RabbitMqBundle\Base\BaseConsumer;

#[Task(queue: 'example_task', exchange: 'tasks')]
class ExampleTaskConsumer extends BaseConsumer
{
    public function execute(string $msg)
    {
        var_dump($msg);
        sleep(1);
    }
}