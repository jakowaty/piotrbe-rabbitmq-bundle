<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Consumer\Topic;

use Piotrbe\RabbitMqBundle\Annotation\Topic;
use Piotrbe\RabbitMqBundle\Base\BaseTopicConsumer;

#[Topic(queue: 'topic__example', exchange: 'exchange_example', routingKey: 'example.*.*')]
class ExampleTopicConsumer extends BaseTopicConsumer
{
    public function execute($msg): void
    {
        var_dump($msg);
        sleep(2);
    }
}