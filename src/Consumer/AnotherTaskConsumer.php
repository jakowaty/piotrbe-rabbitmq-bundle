<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Consumer;

use Piotrbe\RabbitMqBundle\Annotation\Task;
use Piotrbe\RabbitMqBundle\Base\BaseConsumer;

#[Task(queue: 'another_task', exchange: 'tasks')]
class AnotherTaskConsumer extends BaseConsumer
{
    public function execute(string $msg)
    {
        var_dump($msg);
        sleep(1);
    }
}