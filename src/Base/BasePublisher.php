<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;

abstract class BasePublisher extends AMQPConnectionAware
{
    final public function __construct(AMQPStreamConnection $connection, protected QueuesManager $queuesManager)
    {
        parent::__construct($connection);
    }

    public function send(BaseMessage $message): void
    {
        if (!$this->queuesManager->isMessageAllowed($message)) {
            throw new \InvalidArgumentException("Invalid message type");
        }

        $exchangeName = $this->queuesManager->getExchangeForMessage($message);

        $this->channel->queue_declare($message->getType(), false, true, false, false);
        $this->channel->exchange_declare($exchangeName, 'direct', false, true, false);
        $this->channel->queue_bind($message->getType(), $exchangeName, $message->getType());

        $messageAmqp = new AMQPMessage(
            json_encode($message),
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        $this->channel->basic_publish($messageAmqp, $exchangeName, $message->getType());
    }
}