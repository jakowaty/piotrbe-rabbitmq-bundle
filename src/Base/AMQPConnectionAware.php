<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class AMQPConnectionAware
{
    protected AMQPChannel $channel;

    public function __construct(private AMQPStreamConnection $connection) {
        $this->channel = $this->connection?->channel();
    }

    final public function disconnect(): void
    {
        if ($this->channel?->is_open()) {
            $this->channel->close();
            $this->connection?->close();
        }
    }

    final public function __destruct()
    {
        $this->disconnect();
    }
}