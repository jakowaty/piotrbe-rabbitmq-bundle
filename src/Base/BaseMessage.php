<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

abstract class BaseMessage implements \JsonSerializable
{
    public function __construct(protected string $type) {}

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}