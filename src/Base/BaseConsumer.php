<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;

abstract class BaseConsumer extends AMQPConnectionAware
{
    abstract public function execute(string $msg);

    final public function __construct(AMQPStreamConnection $connection, protected QueuesManager $manager) {
        parent::__construct($connection);
    }

    final protected function process(AMQPMessage $msg): void
    {
        try {
            $this->execute($msg->getBody());
            $msg->ack();
        } catch (\Exception $exception) {
            // here Exception thrown from task can be catched
            // and message can be pushed to some kind of Error queue
        }
    }

    final public function work(): void
    {
        $queue = $this->manager->getQueueForConsumer(static::class);
        $exchange = $this->manager->getExchangeForMessageType($queue);

        $this->channel->queue_declare($queue, false, true, false, false);
        $this->channel->basic_qos(null, 1, null);
        $this->channel->exchange_declare($exchange, 'direct', false, true, false);
        $this->channel->queue_bind($queue, $exchange, $queue);
        $this->channel->basic_consume(
            $queue,
            '',
            false,
            false,
            false,
            false,
            [$this, 'process']
        );

        while ($this->channel->is_open()) {
            $this->channel->wait();
        }
    }
}