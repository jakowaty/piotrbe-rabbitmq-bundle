<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Piotrbe\RabbitMqBundle\Service\TopicsRouting;

abstract class BaseTopicConsumer extends AMQPConnectionAware
{
    abstract public function execute($msg): void;

    final public function __construct(AMQPStreamConnection $connection, protected TopicsRouting $topicsRouting)
    {
        parent::__construct($connection);
    }

    final public function process(AMQPMessage $msg): void
    {
        try {
            $this->execute($msg->getBody());
        } catch (\Exception $exception) {
            var_dump($exception);
            // here Exception thrown from task can be catched
            // and message can be pushed to some kind of Error queue
        }
    }

    final public function work(): void
    {
        $exchangeName = $this->topicsRouting->getExchangeForWorker(static::class);
        $queueName = $this->topicsRouting->getQueueForWorker(static::class);

        $this->channel->exchange_declare($exchangeName, 'topic', false, false, false);
        $this->channel->queue_declare($queueName, false, false, true, false);
        $this->channel->queue_bind($queueName, $exchangeName, $this->topicsRouting->getBingingKeyForWorker(static::class));
        $this->channel->basic_consume($queueName, '', false, true, false, false, [$this, 'process']);

        while ($this->channel->is_open()) {
            $this->channel->wait();
        }
    }
}