<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Piotrbe\RabbitMqBundle\Service\TopicsRouting;

abstract class BaseTopicPublisher extends AMQPConnectionAware
{
    final public function __construct(AMQPStreamConnection $connection, protected TopicsRouting $topicsRouting)
    {
        parent::__construct($connection);
    }

    public function send(BaseTopicMessage $message): int
    {
        $exchangesNames = $this->topicsRouting->getExchangesForMessage($message);
        $exchangesSentCount = 0;
        $messageAmqp = new AMQPMessage(
            json_encode($message),
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        foreach ($exchangesNames as $exchangeSingleName) {
            $exchangesSentCount++;
            $this->channel->exchange_declare($exchangeSingleName, 'topic', false, false, false);
            $this->channel->basic_publish($messageAmqp, $exchangeSingleName, $message->getRoutingKey());
        }

        return $exchangesSentCount;
    }
}