<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Base;

abstract class BaseTopicMessage implements \JsonSerializable
{
    public function __construct(protected string $routingKey) {}

    final public function getRoutingKey(): string
    {
        return $this->routingKey;
    }
}