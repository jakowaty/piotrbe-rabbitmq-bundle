<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Annotation;

#[\Attribute(\Attribute::TARGET_CLASS)]
class Task
{
    public function __construct(private string $queue, private string $exchange) {}
}