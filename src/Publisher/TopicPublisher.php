<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Publisher;

use Piotrbe\RabbitMqBundle\Base\BaseTopicPublisher;

class TopicPublisher extends BaseTopicPublisher {}