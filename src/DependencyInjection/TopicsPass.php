<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\DependencyInjection;

use Piotrbe\RabbitMqBundle\Annotation\Topic;
use Piotrbe\RabbitMqBundle\Service\Topic\RoutingKeyMatcher;
use Piotrbe\RabbitMqBundle\Service\TopicsRouting;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TopicsPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $topicRoutingDefinition = $container->getDefinition(TopicsRouting::class);
        $topicConsumersServices = $container->findTaggedServiceIds('topic.consumer');
        $workerToQueueMap = $workerToExchangeMap = $workerToRoutingKeyMap = [];

        foreach ($topicConsumersServices as $id => $tag) {
            $reflection = new \ReflectionClass($id);
            $attribute = current($reflection->getAttributes(Topic::class));

            if (!$attribute) {
                throw new \LogicException(
                    "Class '$id' is tagged '$tag' but has no Task attribute provided"
                );
            }

            $workerToQueueMap[$id] = $attribute->getArguments()['queue'];
            $workerToExchangeMap[$id] = $attribute->getArguments()['exchange'];
            $workerToRoutingKeyMap[$id] = $attribute->getArguments()['routingKey'];
        }

        $topicRoutingDefinition->addArgument($workerToQueueMap);
        $topicRoutingDefinition->addArgument($workerToExchangeMap);
        $topicRoutingDefinition->addArgument($workerToRoutingKeyMap);
        $topicRoutingDefinition->addArgument(
            $container->getDefinition(RoutingKeyMatcher::class)
        );
    }
}