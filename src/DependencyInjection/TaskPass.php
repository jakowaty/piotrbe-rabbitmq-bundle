<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\DependencyInjection;

use Piotrbe\RabbitMqBundle\Annotation\Task;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TaskPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $queuesManagerDefinition = $container->getDefinition(QueuesManager::class);
        $taskServices = $container->findTaggedServiceIds('task.consumer');
        $executionMap = [];
        $exchangeMap = [];
        $typeToExchangeMap = [];

        foreach ($taskServices as $id => $tag) {
            $reflection = new \ReflectionClass($id);
            $attribute = current($reflection->getAttributes(Task::class));

            if (!$attribute) {
                throw new \LogicException(
                    "Class '$id' is tagged '$tag' but has no Task attribute provided"
                );
            }

            $executionMap[$attribute->getArguments()['queue']] = $id;
            $exchangeMap[$id] = $attribute->getArguments()['exchange'];
            $typeToExchangeMap[$attribute->getArguments()['queue']] = $attribute->getArguments()['exchange'];
        }

        $queuesManagerDefinition->addArgument($executionMap);
        $queuesManagerDefinition->addArgument($exchangeMap);
        $queuesManagerDefinition->addArgument($typeToExchangeMap);
    }
}