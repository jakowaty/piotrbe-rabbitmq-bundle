<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Service\Topic;

class RoutingKeyMatcher
{
    public function match(string $routingKey, string $workerKey, string $separator = '.'): bool
    {
        $processedSegmentIndex = 0;
        $match = true;
        $routingKeyArray = explode($separator, $routingKey);
        $workerKeyArray = explode($separator, $workerKey);

        foreach ($workerKeyArray as $el) {

            if ($this->matchToEnd($el)) {
                break;
            }

            if (
                isset($routingKeyArray[$processedSegmentIndex]) &&
                isset($workerKeyArray[$processedSegmentIndex])
            ) {

                if (
                    !$this->matchAnySegment($el) &&
                    !$this->matchSegments($routingKeyArray[$processedSegmentIndex], $workerKeyArray[$processedSegmentIndex])
                ) {
                    $match = false;
                    break;
                }

            } else {
                $match = false;
                break;
            }

            $processedSegmentIndex++;

            if (
                $this->isLastSegment($el, $workerKeyArray) &&
                (count($workerKeyArray) !== count($routingKeyArray))
            ) {
                $match = false;
            }
        }

        return $match;
    }

    private function matchSegments(string $s1, string $s2): bool
    {
        return $s1 === $s2;
    }

    private function matchToEnd(string $segment): bool
    {
        return '#' === $segment;
    }

    private function matchAnySegment(string $segment): bool
    {
        return '*' === $segment;
    }

    private function isLastSegment(string $el, array $workerKeyArray): bool
    {
        return $el === $workerKeyArray[array_key_last($workerKeyArray)];
    }

}