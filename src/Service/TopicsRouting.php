<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Service;

use Piotrbe\RabbitMqBundle\Base\BaseTopicMessage;
use Piotrbe\RabbitMqBundle\Service\Topic\RoutingKeyMatcher;

class TopicsRouting
{
    public function __construct(

        /**
         * @var array
         *      class fqn <STRING> => queue name (like some_queue)
         */
        protected array $workerToQueueMap,

        /**
         * @var array
         *      class fqn <STRING> => exchange name (like some_exchange) <STRING>
         */
        protected array $workerToExchangeMap,

        /**
         * @var array
         *      class fqn <STRING> => routing key (like example.something.*) <STRING>
         */
        protected array $workerToRoutingKeyMap,
        protected RoutingKeyMatcher $keyMatcher
    )
    {
    }

    public function getExchangesForMessage(BaseTopicMessage $message): array
    {
        $exchanges = [];
        $routingKey = $message->getRoutingKey();

        foreach ($this->workerToRoutingKeyMap as $class => $workerKey) {
            if ($this->keyMatcher->match($routingKey, $workerKey)) {
                $exchanges[] = $this->getExchangeForWorker($class);
            }
        }

        return $exchanges;
    }

    public function getExchangeForWorker(string $class): string
    {
        if (!isset($this->workerToExchangeMap[$class])) {
            throw new \InvalidArgumentException("No exchange for worker '$class' id");
        }

        return $this->workerToExchangeMap[$class];
    }

    public function getQueueForWorker(string $class): string
    {
        if (!isset($this->workerToQueueMap[$class])) {
            throw new \InvalidArgumentException("No queue for worker '$class' id");
        }

        return $this->workerToQueueMap[$class];
    }

    public function getBingingKeyForWorker(string $class): string
    {
        if (!isset($this->workerToRoutingKeyMap[$class])) {
            throw new \InvalidArgumentException("No routing key for worker '$class' id");
        }

        return $this->workerToRoutingKeyMap[$class];
    }

    public function isClassForTopicRoutingKey(string $topic): bool
    {
        $rev_workerToRoutingKeyMap = array_flip($this->workerToRoutingKeyMap);

        return array_key_exists($topic, $rev_workerToRoutingKeyMap);
    }

    public function getConsumerClassForRoutingKey(string $topic): string
    {
        $rev = array_flip($this->workerToRoutingKeyMap);

        if (!isset($rev[$topic])) {
            throw new \InvalidArgumentException("No worker class with binding key Attribute: '$topic'");
        }

        return $rev[$topic];
    }

    public function getWorkerToRoutingKeyMap(): array
    {
        return $this->workerToRoutingKeyMap;
    }

}