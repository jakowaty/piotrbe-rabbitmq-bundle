<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Service;

use Piotrbe\RabbitMqBundle\Base\BaseMessage;

/**
 * Holds queues and exchanges data, based on Task attributes.
 *
 * $executionMap = [
 *      queue_name/message_type => consumer_class,
 *      ...
 * ]
 *
 * $exchangeMap = [
 *      consumer_class => exchange_name
 * ]
 *
 * $typeToExchangeMap = [
 *      queue_name/message_type => exchange_name
 * ]
 */
class QueuesManager
{
    private array $executionMapReverted;

    public function __construct(
        private array $executionMap,
        private array $exchangeMap,
        private array $typeToExchangeMap
    ) {
        $this->executionMapReverted = array_flip($this->executionMap);
    }

    public function getQueueForConsumer(string $consumerClassFqn): string {
        if (!isset($this->executionMapReverted[$consumerClassFqn])) {
            throw new \InvalidArgumentException("No Queue defined for consumer '$consumerClassFqn'");
        }

        return $this->executionMapReverted[$consumerClassFqn];
    }

    public function isMessageAllowed(BaseMessage $message): bool
    {
        return $this->isMessageTypeAllowed($message->getType());
    }

    public function isMessageTypeAllowed(string $type): bool
    {
        return in_array($type, array_keys($this->executionMap));
    }

    public function getExchangeForMessage(BaseMessage $message): string
    {
        return $this->getExchangeForMessageType($message->getType());
    }

    public function getExchangeForMessageType(string $type): string
    {
        if (!isset($this->typeToExchangeMap[$type])) {
            throw new \InvalidArgumentException("No exchange defined for type '$type'");
        }

        return $this->typeToExchangeMap[$type];
    }

    public function getConsumerClassForMessageType(string $type): string
    {
        if (!isset($this->executionMap[$type])) {
            throw new \InvalidArgumentException("No consumer defined for type '$type'");
        }

        return $this->executionMap[$type];
    }

    public function getExecutionMap(): array
    {
        return $this->executionMap;
    }

}