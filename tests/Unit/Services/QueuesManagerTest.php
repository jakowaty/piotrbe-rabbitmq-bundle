<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Tests\Unit\Services;

use PHPUnit\Framework\TestCase;
use Piotrbe\RabbitMqBundle\Base\BaseMessage;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;

class QueuesManagerTest extends TestCase
{
    private const executionMap = [
    'type1' => 'consumer1',
    'type2' => 'consumer2',
    'type3' => 'consumer3',
    ];

    private const exchangeMap = [
    'consumer1' => 'exchange1',
    'consumer2' => 'exchange2',
    'consumer3' => 'exchange3',
    ];

    private const typeToExchangeMap = [
    'type1' => 'exchange1',
    'type2' => 'exchange2',
    'type3' => 'exchange3',
    ];

    protected QueuesManager $queuesManager;

    protected function setUp(): void
    {
        parent::setUp();

        $this->queuesManager = new QueuesManager(self::executionMap, self::exchangeMap, self::typeToExchangeMap);
    }

    protected function getBaseMessageStub(string $messageType): BaseMessage
    {
        return new class ($messageType) extends BaseMessage {
            public function jsonSerialize()
            {
                return [
                    'type' => $this->getType()
                ];
            }
        };
    }

    /** @test  */
    public function test_getQueueForConsumer_validQueries(): void
    {
        $this->assertEquals('type1', $this->queuesManager->getQueueForConsumer('consumer1'));
        $this->assertEquals('type2', $this->queuesManager->getQueueForConsumer('consumer2'));
        $this->assertEquals('type3', $this->queuesManager->getQueueForConsumer('consumer3'));
    }

    /** @test  */
    public function test_isMessageAllowed_validMessagesType(): void
    {
        $msg1 = $this->getBaseMessageStub('type1');
        $msg2 = $this->getBaseMessageStub('type2');
        $msg3 = $this->getBaseMessageStub('type3');

        $this->assertTrue($this->queuesManager->isMessageAllowed($msg1));
        $this->assertTrue($this->queuesManager->isMessageAllowed($msg2));
        $this->assertTrue($this->queuesManager->isMessageAllowed($msg3));

        $this->assertTrue($this->queuesManager->isMessageTypeAllowed($msg1->getType()));
        $this->assertTrue($this->queuesManager->isMessageTypeAllowed($msg2->getType()));
        $this->assertTrue($this->queuesManager->isMessageTypeAllowed($msg3->getType()));
    }

    /** @test  */
    public function test_isMessageAllowed_invalidMessagesType(): void
    {
        $msg1 = $this->getBaseMessageStub('type99');
        $msg2 = $this->getBaseMessageStub('type27');
        $msg3 = $this->getBaseMessageStub('type377');

        $this->assertFalse($this->queuesManager->isMessageAllowed($msg1));
        $this->assertFalse($this->queuesManager->isMessageAllowed($msg2));
        $this->assertFalse($this->queuesManager->isMessageAllowed($msg3));

        $this->assertFalse($this->queuesManager->isMessageTypeAllowed($msg1->getType()));
        $this->assertFalse($this->queuesManager->isMessageTypeAllowed($msg2->getType()));
        $this->assertFalse($this->queuesManager->isMessageTypeAllowed($msg3->getType()));
    }

    /** @test */
    public function test_getExchangeForMessage_validData(): void
    {
        $type1 = 'type1';
        $type2 = 'type2';
        $type3 = 'type3';
        $msg1 = $this->getBaseMessageStub($type1);
        $msg2 = $this->getBaseMessageStub($type2);
        $msg3 = $this->getBaseMessageStub($type3);

        $this->assertEquals($this->queuesManager->getExchangeForMessage($msg1), self::typeToExchangeMap[$type1]);
        $this->assertEquals($this->queuesManager->getExchangeForMessage($msg2), self::typeToExchangeMap[$type2]);
        $this->assertEquals($this->queuesManager->getExchangeForMessage($msg3), self::typeToExchangeMap[$type3]);

        $this->assertEquals($this->queuesManager->getExchangeForMessageType($msg1->getType()), self::typeToExchangeMap[$type1]);
        $this->assertEquals($this->queuesManager->getExchangeForMessageType($msg2->getType()), self::typeToExchangeMap[$type2]);
        $this->assertEquals($this->queuesManager->getExchangeForMessageType($msg3->getType()), self::typeToExchangeMap[$type3]);
    }

    /** @test */
    public function test_getExchangeForMessage_invalidData(): void
    {
        $msg = $this->getBaseMessageStub("Invalid_message_type");

        $this->expectException(\InvalidArgumentException::class);
        $this->queuesManager->getExchangeForMessage($msg);
    }

    /** @test */
    public function test_getConsumerClassForMessageType_validData(): void
    {
        $type1 = 'type1';
        $type2 = 'type2';
        $type3 = 'type3';
        $msg1 = $this->getBaseMessageStub($type1);
        $msg2 = $this->getBaseMessageStub($type2);
        $msg3 = $this->getBaseMessageStub($type3);

        $this->assertEquals(self::executionMap[$type1], $this->queuesManager->getConsumerClassForMessageType($msg1->getType()));
        $this->assertEquals(self::executionMap[$type2], $this->queuesManager->getConsumerClassForMessageType($msg2->getType()));
        $this->assertEquals(self::executionMap[$type3], $this->queuesManager->getConsumerClassForMessageType($msg3->getType()));
    }

    /** @test */
    public function test_getConsumerClassForMessageType_invalidData(): void
    {
        $msg = $this->getBaseMessageStub("Invalid_message_type");

        $this->expectException(\InvalidArgumentException::class);
        $this->queuesManager->getConsumerClassForMessageType($msg->getType());
    }
}