<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Tests\Unit\Services\Topic;

use PHPUnit\Framework\TestCase;
use Piotrbe\RabbitMqBundle\Service\Topic\RoutingKeyMatcher;

class RoutingKeyMatcherTest extends TestCase
{
    protected RoutingKeyMatcher $keyMatcher;

    protected function setUp(): void
    {
        parent::setUp();
        $this->keyMatcher = new RoutingKeyMatcher();
    }

    /**
     * @param string $routingKey routing key
     * @param string $workerKey worker key from attribute
     *
     * @test
     * @dataProvider matchingKeysProvider
     */
    public function test_MatchingKeys(string $routingKey, string $workerKey): void
    {
        $this->assertTrue($this->keyMatcher->match($routingKey, $workerKey));
    }

    /**
     * @param string $routingKey routing key
     * @param string $workerKey worker key from attribute
     *
     * @test
     * @dataProvider notMatchingKeysProvider
     */
    public function test_NotMatchingKeys(string $routingKey, string $workerKey): void
    {
        $this->assertFalse($this->keyMatcher->match($routingKey, $workerKey));
    }

    public function matchingKeysProvider(): array
    {
        return [
            ['example.x.y', 'example.x.y'],
            ['example.x.y', 'example.*.*'],
            ['example.x.y', 'example.#'],
            ['example.x.y.z.u', 'example.*.#'],
            ['example.x.y.z.u', '#'],
            ['test.m.y', 'test.*.*']
        ];
    }

    public function notMatchingKeysProvider(): array
    {
        return [
            ['test.master.x.y', 'key.#'],
            ['x.y.z.w', '*.*.*'],
            ['m.t.c.w.q', 'm.t.q.#'],
            ['yo.lo.w.q.d', 'yo.lo.d.*.*']
        ];
    }
}