<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Tests\Unit\Base;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use Piotrbe\RabbitMqBundle\Base\BaseConsumer;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;

class BaseConsumerTest extends TestCase
{
    private string $type = 't1';
    private string $queue = 'q1';
    private string $exchange = 'e1';

    private $queuesManagerMock = null;
    private $amqpConnectionMock = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createValidMocks();
    }

    private function createValidMocks(): void
    {
        $this->queuesManagerMock = $this->getMockBuilder(QueuesManager::class)->disableOriginalConstructor()
            ->getMock();

        $this->queuesManagerMock->expects($this->once())->method('getQueueForConsumer')->withAnyParameters()
            ->willReturn($this->queue);
        $this->queuesManagerMock->expects($this->once())->method('getExchangeForMessageType')->with($this->queue)
            ->willReturn($this->exchange);

        $channel = $this->createMock(AMQPChannel::class);

        $channel->expects($this->once())->method('queue_declare')
            ->with($this->queue, false, true, false, false);
        $channel->expects($this->once())->method('basic_qos')->with(null, 1, null);
        $channel->expects($this->once())->method('exchange_declare')->with($this->exchange, 'direct', false, true, false);;
        $channel->expects($this->once())->method('queue_bind')->with($this->queue, $this->exchange, $this->queue);
        $channel->expects($this->once())->method('basic_consume')->withAnyParameters();
        $channel->expects($this->exactly(2))->method('is_open')->willReturnOnConsecutiveCalls(true, false);
        $channel->expects($this->once())->method('wait')->willReturn(null);


        $this->amqpConnectionMock = $this->getMockBuilder(AMQPStreamConnection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->amqpConnectionMock->expects($this->once())
            ->method('channel')
            ->willReturn($channel);
    }

    private function getAbstractConsumer(): BaseConsumer
    {
        return new class($this->amqpConnectionMock, $this->queuesManagerMock) extends BaseConsumer {
            public function execute(string $msg)
            {
                print $msg;
            }
        };
    }

    /** @test */
    public function test_work(): void
    {
        $consumer = $this->getAbstractConsumer();
        $consumer->work();
    }
}