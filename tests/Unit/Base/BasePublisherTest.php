<?php declare(strict_types = 1);

namespace Piotrbe\RabbitMqBundle\Tests\Unit\Base;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPUnit\Framework\TestCase;
use Piotrbe\RabbitMqBundle\Base\BasePublisher;
use Piotrbe\RabbitMqBundle\Message\ExampleTask;
use Piotrbe\RabbitMqBundle\Service\QueuesManager;

class BasePublisherTest extends TestCase
{
    protected $connection;
    protected $queuesManager;
    protected $message;

    protected function setUp_Mocks_test_send_message_valid_data_normal_path(): void
    {
        $exchangeName = 'tasks';
        $messageToSend = new ExampleTask("text");
        $queuesManagerMock = $this->createMock(QueuesManager::class);
        $channelMock = $this->createMock(AMQPChannel::class);
        $connectionMock = $this->createMock(AMQPStreamConnection::class);

        $queuesManagerMock
            ->expects($this->once())
            ->method('getExchangeForMessage')
            ->with($messageToSend)
            ->willReturn($exchangeName);

        $queuesManagerMock
            ->expects($this->once())
            ->method('isMessageAllowed')
            ->with($messageToSend)
            ->willReturn(true);

        $channelMock->expects($this->once())
            ->method('queue_declare')
            ->with($messageToSend->getType(), false, true, false, false);

        $channelMock->expects($this->once())
            ->method('exchange_declare')
            ->with($exchangeName, 'direct', false, true, false);

        $channelMock->expects($this->once())
            ->method('queue_bind')
            ->with($messageToSend->getType(), $exchangeName, $messageToSend->getType());

        $channelMock->expects($this->once())
            ->method('basic_publish')
            ->withAnyParameters();

        $connectionMock->expects($this->once())
            ->method('channel')
            ->willReturn($channelMock);

        $this->queuesManager = $queuesManagerMock;
        $this->connection = $connectionMock;
        $this->message = $messageToSend;
    }

    protected function setUp_Mocks_test_send_message_invalid_message_type_path(): void
    {
        $messageToSend = new ExampleTask("text");
        $queuesManagerMock = $this->createMock(QueuesManager::class);
        $channelMock = $this->createMock(AMQPChannel::class);
        $connectionMock = $this->createMock(AMQPStreamConnection::class);

        $queuesManagerMock
            ->expects($this->once())
            ->method('isMessageAllowed')
            ->with($messageToSend)
            ->willReturn(false);

        $connectionMock->expects($this->once())
            ->method('channel')
            ->willReturn($channelMock);

        $this->queuesManager = $queuesManagerMock;
        $this->connection = $connectionMock;
        $this->message = $messageToSend;
    }

    protected function getAbstractPublisher(): BasePublisher
    {
        return new class($this->connection, $this->queuesManager) extends BasePublisher {};
    }

    /** @test */
    public function test_send_message_invalid_message_type_path(): void
    {
        $this->setUp_Mocks_test_send_message_invalid_message_type_path();
        $this->expectException(\InvalidArgumentException::class);

        $basePublisher = $this->getAbstractPublisher();
        $basePublisher->send($this->message);
    }

    /** @test */
    public function test_send_message_valid_data_normal_path(): void
    {
        $this->setUp_Mocks_test_send_message_valid_data_normal_path();

        $basePublisher = $this->getAbstractPublisher();
        $basePublisher->send($this->message);
    }
}